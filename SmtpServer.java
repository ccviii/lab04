import java.io.* ;
import java.net.* ;
import java.util.* ;

public class SmtpServer {
	private String from, server, to, content;
	private int step;
	private int OK=250, SYNTAX=555, UNKNOWN=502;
	private int HELO=0, MAILFROM=1, RCPT=2, DATA=3, DATA2=4, DONE=5;
	private UUID id;
	private ServerSocket serverSocket;
	private PrintWriter out;

	/*
	 * - - - constructores - - - -
	 */
	public SmtpServer(int port, String server){
		this.from = "";
		this.to = "";
		this.content = "";
		this.server = server;
		this.step = 0;
		this.id = UUID.randomUUID();
		
		try { 
			this.serverSocket = new ServerSocket(port); 
		} catch (IOException e) {
			System.err.println("\tCould not listen on port: "+port); 
			System.exit(1); 
		}
	}
	public SmtpServer(){
		this(1337, "mx.server.demo");
	}
	public SmtpServer(String server){
		this(1337, server);
	}
	public SmtpServer(int port){
		this(port, "mx.server.demo");
	}

	
	/*
	 * - - - Metodos - - - -
	 */
	public void helo(String line){
		nextStep(MAILFROM);
		out.println("250 "+this.server+" at your service");
	}
	public void mailFrom(String line){
		if(this.step<MAILFROM) { this.exit(503); return; }

		if(SmtpServer.contains(line, "<") &&
			SmtpServer.contains(line, ">") && 
			SmtpServer.contains(line, ":") && 
			SmtpServer.contains(line, "@")) {
			this.exit(OK);
			this.from = line.substring(line.indexOf(": ")+2);
			nextStep(RCPT);
		} else {
			this.step = 1;
			this.exit(SYNTAX);
		}
	}
	public void rcpt(String line){
		if(this.step<RCPT) { this.exit(503); return; }
		if(SmtpServer.contains(line, " to:")) {
			exit(OK);
			this.to = line.substring(line.indexOf(": ")+2);
			nextStep(DATA);
		} else
			exit(SYNTAX);
	}
	public void data(String line){
		if(this.step<DATA) { this.exit(503); return; }
		if(line.equals("."))
			nextStep(DONE);
		else
			this.content += line+"\n";
	}
	public void checkSequence(){
		if(this.step<DATA) { this.exit(503); return; }
		out.println("GO AHEAD");
		nextStep(DATA2);
	}


	/*
	 * - - - Extras - - - -
	 */
	public void nextStep(int next){
		if((this.step==next-1))
			println("next: "+next);
		this.step = (this.step==next-1)? next: step;
	}
	public static boolean contains(String line, String value){
		return (line.indexOf(value)!=-1);
	}
	public void exit(int code){
		String phase = "";

		switch (code) {
			case 555:
				out.println("555 5.5.2 Syntax error.");
				break;
			case 503:
				phase = (this.step<MAILFROM)? "EHLO/HELO": "MAIL";
				phase = (this.step==RCPT)? "RCPT": phase;
				out.println("503 5.5.1 Error: "+phase+" first.");
				break;
			case 502:
				out.println("502 5.5.1 Unrecognized command.");
				break;
			case 250:
				out.println("250 2.1.0 OK.");
				break;
		}
	}
	public void print(Object s){
		System.out.print(s);
	}

	public void println(Object s){
		System.out.println(s);
	}

	/*
	 * - - - MAIN - - - -
	 */
	public void start() throws Exception{
		// campos
		// Scanner sc = new Scanner(System.in);
		String line = "";
		int pos = -1;

		// TCP
		Socket clientSocket = null; 
		System.out.println ("\tWaiting for connection.....");

		try { 
			clientSocket = serverSocket.accept(); 
		} catch (IOException e)  {
			System.err.println("Accept failed.");
			System.exit(1);
		}

		this.out = new PrintWriter(clientSocket.getOutputStream(), true);
		BufferedReader in = new BufferedReader(new InputStreamReader( clientSocket.getInputStream()));

		// System.out.print("> ");
		// out.println("Trying ...");
		// out.println("Connected to "+this.server);
		// out.println("Escape character is 'q'.");

		while(this.step!=DONE && !(line = in.readLine()).equals("q")){
			if(SmtpServer.contains(line, "data")) checkSequence();
			else if(this.step==DATA2) data(line);
			else if(SmtpServer.contains(line, "helo")) helo(line);
			else if(SmtpServer.contains(line, "mail from")) mailFrom(line);
			else if(SmtpServer.contains(line, "rcpt")) rcpt(line);
			else this.exit(UNKNOWN);
		}

		out.println(this);
		println("done");

		try {
			out.close(); 
			in.close(); 
			clientSocket.close(); 
			serverSocket.close();
		} catch (Exception e) {
			println("fail");
			println(e);
			System.exit(1);
		}
	}

	public String toString(){
		String message = "\n";
		if(from.equals("") || to.equals("") || content.equals("")){
			message += "Email not sent!, missing params.";
		} else {
			message += "Email sent!\n";
			message += "FROM: "+this.from+"\n";
			message += "TO: "+this.to+"\n";
			message += this.content + '\n';
			message += "EMAIL ID: "+this.id+"\n";
		}
		return message;
	}
}