import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Example {
    public static void main(String[] args) throws Exception {
        ExecutorService executor = Executors.newFixedThreadPool(10);
        // for (int i = 0; i < 10; i++) {
        ServerSocket serverSocket = new ServerSocket(1337);
        int id = 0;
        while(true){
        	Socket client = serverSocket.accept();
        	Runnable worker = new WorkerThread(""+id, client);
            executor.execute(worker);
            executor.shutdown();
            id++;
        }
        
        // System.out.println("Finished all threads");
    }

}

class WorkerThread implements Runnable {

    private String command;
    private Socket client;

    public WorkerThread(String s, Socket client){
        this.command=s;
        this.client = client;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+" Start. Command = "+command);
        processCommand();
        System.out.println(Thread.currentThread().getName()+" End.");
    }

    private void processCommand() {
        try {
        	this.client.close();
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch(Exception e){
        	e.printStackTrace();
        }
    }

    @Override
    public String toString(){
        return this.command;
    }
}