make: server

run: server start

server:
	clear
	javac SmtpServer.java
	javac App.java

start:
	java App

clean:
	rm *.class

